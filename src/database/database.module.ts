import { Module } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import config from 'src/config';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      name: 'pg',
      inject: [config.KEY],
      useFactory: (configService: ConfigType<typeof config>) => {
        const { user, host, dbName, password, port } = configService.postgres;
        return {
          name: 'pg',
          type: 'postgres',
          host,
          port,
          username: user,
          password,
          database: dbName,
          // entities: ['src/**/*.entity.ts'],
          synchronize: false,
          autoLoadEntities: true,
        };
      },
    }),
  ],
  providers: [],
  exports: [TypeOrmModule],
})
export class DatabaseModule {}
