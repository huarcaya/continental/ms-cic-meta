import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { MenuEntity } from './entities/menu.entity';
import { MenusController } from './controllers/menus/menus.controller';
import { MenusService } from './services/menus/menus.service';
import { ModuleEntity } from './entities/module.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MenuEntity, ModuleEntity], 'pg')],
  controllers: [MenusController],
  providers: [MenusService],
})
export class MenusModule {}
