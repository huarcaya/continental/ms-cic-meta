import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { MenuEntity } from 'src/menus/entities/menu.entity';
import { MenuDto } from 'src/menus/dtos/menu.dto';

@Injectable()
export class MenusService {
  constructor(
    @InjectRepository(MenuEntity, 'pg')
    private menuRepository: Repository<MenuEntity>,
  ) {}

  async findAllAsync() {
    return this.menuRepository.find({
      relations: ['module', 'module.parent', 'parent'],
    });
  }

  async getAllMenusByHierarchy(): Promise<MenuDto[]> {
    const allMenus: MenuDto[] = await this.findAllAsync();

    const menus: MenuDto[] = allMenus.filter((item) => item.parent === null);
    const subMenus = allMenus.filter((item) => !!item.parent);

    for (const menu of menus) {
      menu.menus = subMenus.filter((item) => item.parent.id === menu.id);
      for (const sMenu of menu.menus) {
        sMenu.menus = subMenus.filter((item) => item.parent.id === sMenu.id);
      }
    }

    return menus;
  }
}
