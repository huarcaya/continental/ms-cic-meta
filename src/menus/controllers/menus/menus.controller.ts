import { Controller, Get } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

import { MenusService } from 'src/menus/services/menus/menus.service';

@Controller('menus')
export class MenusController {
  constructor(private menusService: MenusService) {}

  @Get()
  @MessagePattern({ cmd: 'cic-menu' })
  async findAllAsync() {
    return this.menusService.getAllMenusByHierarchy();
  }
}
