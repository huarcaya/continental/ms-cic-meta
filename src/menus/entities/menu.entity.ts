import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  Unique,
} from 'typeorm';
import { ModuleEntity } from './module.entity';

@Entity({ name: 'menus', schema: 'cic', database: 'pg' })
@Unique(['slug', 'module', 'parent', 'moduleIndex'])
export class MenuEntity {
  @PrimaryColumn({
    name: 'menu_id',
  })
  id: number;

  @ManyToOne(() => ModuleEntity, {
    nullable: false,
    onUpdate: 'CASCADE',
    onDelete: 'RESTRICT',
  })
  @JoinColumn({ name: 'module_id', referencedColumnName: 'id' })
  module: ModuleEntity;

  @Column({
    name: 'menu_name',
    type: 'varchar',
    length: 255,
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 255,
  })
  slug: string;

  @Column({ name: 'module_index', type: 'boolean', default: false })
  moduleIndex: boolean;

  @Column({
    name: 'menu_icon',
    type: 'varchar',
    length: 25,
    nullable: true,
  })
  icon: string;

  @Column({
    name: 'menu_location',
    type: 'varchar',
    length: 10,
    enum: ['all', 'main', 'sidebar'],
  })
  location: string;

  @Column({
    name: 'menu_order',
    default: 0,
  })
  order: number;

  @ManyToOne(() => MenuEntity, { onUpdate: 'CASCADE', onDelete: 'RESTRICT' })
  @JoinColumn({ name: 'menu_parent_id' })
  parent?: MenuEntity;

  @Column({
    name: 'is_enabled',
    default: true,
  })
  isEnabled: boolean;

  @Column({
    name: 'is_external',
    default: false,
  })
  isExternal: boolean;
}
