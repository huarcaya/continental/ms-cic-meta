import {
  Column,
  Entity,
  ManyToOne,
  JoinColumn,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity({ name: 'modules', schema: 'cic' })
export class ModuleEntity {
  @PrimaryColumn({ name: 'module_id', type: 'varchar', length: 25 })
  id: string;

  @Column({ name: 'app_id', type: 'varchar', length: 25 })
  appId: string;

  @Column({ name: 'module_name', type: 'varchar', length: 255 })
  name: string;

  @Column({ name: 'module_description', type: 'varchar', length: 500 })
  description: string;

  @ManyToOne(() => ModuleEntity, { onUpdate: 'CASCADE', onDelete: 'RESTRICT' })
  @JoinColumn({ name: 'module_parent_id' })
  parent?: ModuleEntity;

  @CreateDateColumn({ name: 'created_at', type: 'timestamp without time zone' })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamp without time zone',
    nullable: true,
  })
  updatedAt?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    type: 'timestamp without time zone',
    nullable: true,
  })
  deletedAt?: Date;
}
