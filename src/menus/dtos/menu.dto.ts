import { MenuEntity } from '../entities/menu.entity';
import { ModuleEntity } from '../entities/module.entity';

export class MenuDto {
  id: number;

  module: ModuleEntity;

  name: string;

  slug: string;

  parent?: MenuEntity;

  isExternal: boolean;

  order: number;

  location: string;

  icon: string;

  isEnabled: boolean;

  moduleIndex: boolean;

  menus?: MenuDto[];
}
